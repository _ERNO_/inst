@extends('layouts.app')

@section('content')
<div class="container" style="min-width: 700px">
    <div class="row">
        <div class="col">
            <table class="table">
                <tbody>
                @foreach(\Illuminate\Support\Facades\Auth::user()->subscribers as $sub)
                <tr>

                    <td>
                        {{$sub->name}}<br>
                        @foreach($sub->posts as $post)

                            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    @foreach($post->pictures as $key => $picture)
                                    <div class="carousel-item {{$key == 0 ? 'active' : ''}}">
                                        <img src="{{asset('/storage/' . $picture->picture)}}" class="d-block w-100" alt="{{$picture->picture}}">
                                    </div>
                                    @endforeach

                                </div>
                                <button class="carousel-control-prev" type="button" data-target="#carouselExampleControls" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </button>
                                <button class="carousel-control-next" type="button" data-target="#carouselExampleControls" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </button>
                            </div>
                                <br>
                                @foreach($post->comments as $comment)
                                    {{$loop->iteration}} {{$comment->body}}<br><br>
                                @endforeach
                            <div class="row">
                                <div class="col">
                                    <form action="{{action([\App\Http\Controllers\CommentsController::class, 'store'], ['post' => $post])}}" method="post">
                                        @csrf
                                        <div class="from-group">
                                            <input name="post_id" hidden type="text" value="{{$post->id}}">
                                            <textarea name="body" id="body" cols="65" rows="2">{{old('body')}}</textarea>
                                            @error('body')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <button value="submit" class="btn btn-sm btn-outline-success">
                                            добавить комментарии
                                        </button>
                                    </form>
                                </div>
                            </div>
                        @endforeach
                    </td>

                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
