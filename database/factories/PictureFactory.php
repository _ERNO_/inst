<?php

namespace Database\Factories;


use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;


class PictureFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'picture' => $this->getPhoto(rand(1, 20)),
            'post_id' => rand(1, 5),
        ];
    }

    /**
     * @param int $number
     * @return string
     */
    private function getPhoto(int $number = 1):string
    {
        $path = storage_path() . "/photo/" . $number . ".jpg";
        $image_name = md5($path) . ".jpg";
        $resize = Image::make($path)->fit(300)->encode('jpg');
        Storage::disk('public')->put('photos/' . $image_name, $resize->__toString());
        return 'photos/' . $image_name;
    }

}
