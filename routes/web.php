<?php

use App\Http\Controllers\CommentsController;
use App\Http\Controllers\PicturesController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [UsersController::class, 'index']);

Auth::routes();
Route::resource('users', UsersController::class)->except(['create', 'store']);
Route::get('/users/{user}/tape', [UsersController::class, 'tape'])->name('users.tape');
Route::resource('pictures', PicturesController::class)->only('store');
Route::resource('comments', CommentsController::class)->only('store');
Route::post('/users/{user}/index', [UsersController::class, 'subscribe']);
Route::delete('/users/{user}/index', [UsersController::class, 'unsubscribe']);
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
