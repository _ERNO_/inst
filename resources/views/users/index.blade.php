@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col">
                <div class="row">
                    <div class="col">
                        <div>
                            <a class="btn btn-outline-success" href="{{action([\App\Http\Controllers\UsersController::class, 'tape'], ['user' => \Illuminate\Support\Facades\Auth::user()])}}">Моя лента</a>
                            <a class="btn btn-outline-success" href="{{route('users.show', ['user' => \Illuminate\Support\Facades\Auth::user()])}}">Профиль</a>
                        </div>
                    </div>
                </div>
                <table class="table">
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                                    <td>
                                        <a href="{{route('users.show', compact('user'))}}">{{$user->name}}</a>
                                    </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
