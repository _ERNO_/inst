<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;


class CommentsController extends Controller
{

    /**
     * @param CommentRequest $request
     * @return RedirectResponse
     */
    public function store(CommentRequest $request): RedirectResponse
    {

        $comment = new Comment($request->all());
        $comment->user_id = Auth::id();
        $comment->save();
       return  redirect()->back();
    }
}
