<?php

namespace App\Http\Controllers;

use App\Http\Requests\PictureRequest;
use App\Models\Picture;
use App\Models\User;

use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;


class PicturesController extends Controller
{


    public function store(PictureRequest $request): RedirectResponse
    {
        $data = $request->all();
        $file = $request->file('picture');
        if (!is_null($file)){
            $path = $file->store('photos', 'public');
            $data['picture'] = $path;
        }
        $picture = new Picture($data);
        $picture->user_id = Auth::id();
        $picture->save();
        return redirect()->route('users.show', ['user' => Auth::user()])->with('status', "successfully created");
    }
}
