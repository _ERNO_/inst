@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col">
          @if($user->id == \Illuminate\Support\Facades\Auth::id())
            <form action="{{action([\App\Http\Controllers\PicturesController::class, 'store'], ['picture' => $picture])}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="custom-file">
                    <label class="custom-file-label" for="customFile">Добавить фото</label>
                    <input type="file" class="@error('picture') is-invalid @enderror custom-file-input" id="picture" name="picture" value="{{old('picture')}}">
                    @error('picture')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <br><br>
                <button type="submit">Добавить</button>
            </form>
            <br>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col">
            {{$user->name}}
            <div>
                @if(\Illuminate\Support\Facades\Auth::id() != $user->id and $user->id != $sub)

                    <form action="{{action([\App\Http\Controllers\UsersController::class, 'subscribe'], ['user' => $user])}}" method="post">
                        @csrf
                        <button class="btn btn-sm btn-outline-success" name="submit">
                            <b>подписаться</b>
                        </button>
                    </form>
                @else
                    @if(\Illuminate\Support\Facades\Auth::id() != $user->id)
                    <form action="{{action([\App\Http\Controllers\UsersController::class, 'unsubscribe'], ['user' => $user])}}" method="post">
                        @method('delete')
                        @csrf
                        <button class="btn btn-sm btn-outline-danger" name="submit">
                            <b>отменить подписку</b>
                        </button>
                    </form>
                    @endif
                @endif
            </div>
            @foreach($user->posts as $post)
                <table class="table">
                    <tbody>
                    <tr>
                        <td>

                            <div id="carouselExampleFade" class="carousel slide carousel-fade" data-bs-ride="carousel">
                                <div class="carousel-inner">
                                    @foreach($post->pictures as $key => $picture)
                                        <div class="carousel-item {{$key == 0 ? 'active' : ''}}">
                                            <img src="{{asset('/storage/' . $picture->picture)}}" class="d-block w-100" alt="{{$picture->picture}}">
                                        </div>
                                    @endforeach
                                </div>
                                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Previous</span>
                                </button>
                                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Next</span>
                                </button>
                            </div>
                            <br>
                                @foreach($post->comments as $com)
                                {{$loop->iteration}} {{$com->body}} <br><br>
                                @endforeach
                            <div class="row" style="margin-top: 10px">
                                <div class="col">
                                    <form action="{{action([\App\Http\Controllers\CommentsController::class, 'store'], ['post' => $post])}}" method="post">
                                        @csrf
                                        <div class="from-group">
                                            <input name="post_id" hidden type="text" value="{{$post->id}}">
                                            <textarea name="body" id="body" cols="65" rows="2">{{old('body')}} @error('body')  @enderror</textarea>
                                            @error('body')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <button value="submit" class="btn btn-sm btn-outline-success">
                                            добавить комментарии
                                        </button>
                                    </form>
                                </div>
                            </div>

                        </td>

                    </tr>
                    </tbody>
                </table>
            @endforeach
        </div>
    </div>
</div>
@endsection
