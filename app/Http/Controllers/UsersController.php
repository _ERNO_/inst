<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Picture;
use App\Models\Post;
use App\Models\User;

use App\Models\UserUser;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {

        $users = User::all();
        return view('users.index', compact('users'));
    }


    /**
     * @param User $user
     * @return Application|Factory|View
     */
    public function show(User $user, Picture $picture, Comment $comment)
    {
        $sub = 0;
        $user_subs = Auth::user()->subscribers->pluck('id');
       $arr = $user_subs->toArray();
        if ($arr != []){
            foreach ($user_subs as $subs){
                if ($subs == $user->id){
                    $sub = $subs;
                }
            }
        }

        return view('users.show', compact('user', 'picture', 'sub', 'comment'));
    }


    /**
     * @param User $user
     * @return Application|Factory|View
     */
    public function edit(User $user)
    {
        return view('users.edit', compact('user'));
    }



    public function update(Request $request, User $user): RedirectResponse
    {
       $request->validate([
           'name' => ['required', 'max:128'],
           'email' => ['required', 'unique:App\Models\User,email'],
           'password' => ['required', 'string', 'min:8', 'confirmed'],
       ]);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->update();
        return redirect()->route('users.show', compact('user'))->with('success', 'successfully updated');
    }


    /**
     * @param User $user
     * @return RedirectResponse|mixed
     */
    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->route('login')-with('success', 'successfully deleted');
    }

    /**
     * @param User $user
     * @return RedirectResponse
     */
    public function subscribe(User $user): RedirectResponse
    {
        $auth_user = Auth::user();

        $auth_user->subscribers()->save($user);
        $auth_user->refresh();

        return redirect()->route('users.show', compact('user'));
    }

    /**
     * @param User $user
     * @return RedirectResponse
     */
    public function unsubscribe(User $user): RedirectResponse
    {
        $auth_user = Auth::user();
        $auth_user->subscribers()->detach($user);
        $auth_user->refresh();
        return redirect()->route('users.show', compact('user'));
    }

    public function tape(User $user, Comment $comment)
    {
        return view('users.user_type', compact('user', 'comment'));
    }

}
